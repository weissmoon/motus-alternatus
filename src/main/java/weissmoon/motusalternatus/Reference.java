package weissmoon.motusalternatus;

public class Reference{
    public static final String MOD_ID = "motusalternatus";
    public static final String MOD_NAME = "Motus Alternatus";
    public static final String VERSION = "0.0.5";
    public static final String AUTHOR = "Weissmoon";
    public static final String GUI_FACTORY_CLASS = "weissmoon.motusalternatus.client.gui.GuiFactory";
}
