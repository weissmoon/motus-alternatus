package weissmoon.motusalternatus;

import net.minecraft.block.material.Material;

import java.util.LinkedHashMap;

/**
 * Created by Weissmoon on 4/7/19.
 */
public class MaterialRegistry {

    public static final MaterialRegistry INSTANSE = new MaterialRegistry();

    private LinkedHashMap<String, Material> MaterialList = new LinkedHashMap();

    public void putMaterial(String name, Material material){
        MaterialList.put(name, material);
    }

    public boolean isMaterialRegistered(Material material){
        return !(MaterialList.get(material) == null);
    }
}
