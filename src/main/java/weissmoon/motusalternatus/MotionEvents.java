package weissmoon.motusalternatus;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AbstractAttributeMap;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

import java.util.LinkedHashMap;
import java.util.UUID;

/**
 * Created by Weissmoon on 10/29/18.
 */
public class MotionEvents {

    private LinkedHashMap<Material, Double> MaterialList = new LinkedHashMap();
    private LinkedHashMap<Block, Double> ExceptionList = new LinkedHashMap();
    private LinkedHashMap<String, Double> PlayerList = new LinkedHashMap();
    static final UUID uuid = UUID.fromString("195fc0db-6130-4d49-979d-3b64754af6b3");
    //29d2b7de-c2dd-4d16-a401-190a7b34eb0d
    public MotionEvents(){
        MaterialList.put(Material.CARPET, 1.2);
        MaterialList.put(Material.CLOTH, 1.2);
        MaterialList.put(Material.ROCK, 1.2);
        MaterialList.put(Material.GLASS, 1.2);

        MaterialList.put(Material.GROUND, 0.95);

        MaterialList.put(Material.CRAFTED_SNOW, 0.8);
        MaterialList.put(Material.SAND, 0.75);
        MaterialList.put(Material.SNOW, 0.7);
        MaterialList.put(Material.SPONGE, 0.75);
        MaterialList.put(Material.PLANTS, 0.8);
        MaterialList.put(Material.LEAVES, 0.8);
        MaterialList.put(Material.VINE, 0.8);

        for(String s : ConfigurationHandler.blockConfigurationList){
            try {
                String name = s.substring(0, s.indexOf("|"));
                if(Item.getByNameOrId(name) instanceof ItemBlock){

                    String[] doublee = s.split("\\|");

                    double r = new Double(doublee[1]);
                    if(r < 0.35) {
                        r = 0.35;
                        MotusAlternatus.instance.log.info("Cadd");
                        MotusAlternatus.instance.log.info(s);
                    }
                    ExceptionList.put(((ItemBlock) Item.getByNameOrId(name)).getBlock(), r);
                }
            }catch (Exception e){
                MotusAlternatus.instance.log.catching(e);
                MotusAlternatus.instance.log.error(s);
            }
        }
        //Old worldgen used gravel for villages
        //ExceptionList.put(Blocks.GRAVEL, 1.0);
    }


    @SubscribeEvent
    public void onPlayerMotion(TickEvent.PlayerTickEvent event){
        EntityPlayer player = event.player;
        double motion = 1;
        if(player.onGround){

            //if(player.isInWater())return;

            double alter = 0;
            boolean floor = false;
            int j6 = MathHelper.floor(player.posX);
            int i1 = MathHelper.floor(player.posY - 0.20000000298023224D);
            int k6 = MathHelper.floor(player.posZ);

            BlockPos blockpos = new BlockPos(j6, i1, k6);
            IBlockState iblockstate = player.world.getBlockState(blockpos);
            Block block = iblockstate.getBlock();

            Material material;

            if(ExceptionList.containsKey(block)){
                alter += ExceptionList.get(block);
                floor = true;
            }else{
                material = iblockstate.getMaterial();
                if(MaterialList.containsKey(material)){
                    alter += MaterialList.get(material);
                    floor = true;
                }
            }

//            allows for transparent materials
//            if(canSustainAnyPlant(iblockstate)) {
                iblockstate = player.world.getBlockState(new BlockPos(blockpos).up());
                material = iblockstate.getMaterial();
                if (MaterialList.containsKey(material)) {
                    alter += MaterialList.get(material);
                    if (floor)
                        alter /= 2.0;
                }
//            }
            if(alter != 0)
                motion *= alter;
        }
        if(!player.world.isRemote) {
            if (PlayerList.get(player.getGameProfile().getName()) != motion) {
                PlayerList.put(player.getGameProfile().getName(), motion);
                AbstractAttributeMap playerAttributeMap = player.getAttributeMap();
                IAttributeInstance iattributeinstance = playerAttributeMap.getAttributeInstance(SharedMonsterAttributes.MOVEMENT_SPEED);

                iattributeinstance.removeModifier(uuid);
                iattributeinstance.applyModifier(new AttributeModifier(uuid,
                    uuid.toString(),
                    motion - 1,
                    2));
            }
        }
//        player.motionX *= motion;
//        player.motionZ *= motion;
    }

    @SubscribeEvent
    public void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event){
        PlayerList.put(event.player.getGameProfile().getName(), 1.0);
    }

    @SubscribeEvent
    public void onPlayerLogout(PlayerEvent.PlayerLoggedOutEvent event){
        event.player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.MOVEMENT_SPEED).removeModifier(uuid);
        PlayerList.remove(event.player.getGameProfile().getName());
    }

    private boolean canSustainAnyPlant(IBlockState iBlockState){
        if(iBlockState.getBlock() == Blocks.GRASS ||
                iBlockState.getBlock() == Blocks.SAND ||
                iBlockState.getBlock() == Blocks.CLAY ||
                iBlockState.getBlock() == Blocks.HARDENED_CLAY ||
                iBlockState.getBlock() == Blocks.STAINED_HARDENED_CLAY ||
                iBlockState.getBlock() == Blocks.SOUL_SAND ||
                iBlockState.getBlock() == Blocks.FARMLAND ||
                iBlockState.getBlock() == Blocks.DIRT)
            return true;
        return false;
    }
}
