package weissmoon.motusalternatus;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.io.File;

/**
 * Created by Weissmoon on 10/29/18.
 */
public class ConfigurationHandler {

    public static Configuration configuration;

    public static String[] materialConfigurationList;
    public static String[] blockConfigurationList;

    public static boolean noFOV;

    public static String CATEGORY_CUSTOM = "customOverrides";

    public static void init (File configFile){
        if (configuration == null){
            configuration = new Configuration(configFile);
            loadCofiguration();
        }
    }

    private static void loadCofiguration (){

        blockConfigurationList = configuration.getStringList("blockConfigurationList list", CATEGORY_CUSTOM,
                new String[]{
                    "minecraft:gravel|1.0"
                },
                "Block Override, must be minimum 0.35");
        noFOV = configuration.getBoolean("noFOV", Configuration.CATEGORY_CLIENT, false, "Diable FOV change for everything");
        if (configuration.hasChanged()){
            configuration.save();
        }
    }

    @SubscribeEvent
    public static void onConfigurationChangedEvent (ConfigChangedEvent.OnConfigChangedEvent event){
        if (event.getModID().equalsIgnoreCase("motusalternatus")){
            loadCofiguration();
            MotusAlternatus.instance.handleFOVSwitch();
        }
    }

    public static void set (String categoryName, String propertyName, String newValue){
        configuration.load();
        if ((configuration.getCategoryNames().contains(categoryName)) && (configuration.getCategory(categoryName).containsKey(propertyName))){
            configuration.getCategory(categoryName).get(propertyName).set(newValue);
        }

        configuration.save();
    }
}
