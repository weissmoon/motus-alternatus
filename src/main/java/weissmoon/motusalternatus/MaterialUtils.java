//package weissmoon.motusalternatus;
//
//import net.minecraft.block.material.Material;
//
//import java.util.LinkedHashMap;
//
///**
// * Created by Weissmoon on 5/4/19.
// */
//public class MaterialUtils {
//
//    private static LinkedHashMap<String, Material> vanillaMaterialMap = new LinkedHashMap();
//    private static LinkedHashMap<String, Material> tecnichalMaterialMap = new LinkedHashMap();
//    public LinkedHashMap<String, Material> materialMap = new LinkedHashMap();
//
//    static{
//        vanillaMaterialMap.put("air", Material.AIR)
//        vanillaMaterialMap.put("grass",  Material.GRASS)
//        vanillaMaterialMap.put("ground", Material.GROUND)
//        vanillaMaterialMap.put("wood", Material.WOOD)
//        vanillaMaterialMap.put("rock", Material.ROCK)
//        vanillaMaterialMap.put("iron", Material.IRON)
//        vanillaMaterialMap.put("anvil", Material.ANVIL)
//        vanillaMaterialMap.put("water", Material.WATER)
//        vanillaMaterialMap.put("lava", Material.LAVA)
//        vanillaMaterialMap.put("leaves", Material.LEAVES)
//        vanillaMaterialMap.put("plants", Material.PLANTS)
//        vanillaMaterialMap.put("vine", Material.VINE)
//        vanillaMaterialMap.put("sponge", Material.SPONGE)
//        vanillaMaterialMap.put("cloth", Material.CLOTH)
//        vanillaMaterialMap.put("fire", Material.FIRE)
//        vanillaMaterialMap.put("sand", Material.SAND)
//        vanillaMaterialMap.put("circuits", Material.CIRCUITS)
//        vanillaMaterialMap.put("carpet", Material.CARPET)
//        vanillaMaterialMap.put( Material.GLASS)
//        vanillaMaterialMap.put( Material.REDSTONE_LIGHT)
//        vanillaMaterialMap.put( Material.TNT)
//        vanillaMaterialMap.put( Material.CORAL)
//        vanillaMaterialMap.put( Material.ICE)
//        vanillaMaterialMap.put( Material.PACKED_ICE)
//        vanillaMaterialMap.put( Material.SNOW)
//        vanillaMaterialMap.put( Material.CRAFTED_SNOW)
//        vanillaMaterialMap.put( Material.CACTUS)
//        vanillaMaterialMap.put( Material.CLAY)
//        vanillaMaterialMap.put( Material.GOURD)
//        vanillaMaterialMap.put( Material.DRAGON_EGG)
//        vanillaMaterialMap.put( Material.PORTAL)
//        vanillaMaterialMap.put( Material.CAKE)
//        vanillaMaterialMap.put( Material.WEB)
//        vanillaMaterialMap.put( Material.PISTON)
//        vanillaMaterialMap.put( Material.BARRIER)
//        vanillaMaterialMap.put( Material.STRUCTURE_VOID)
//    }
//}
