package weissmoon.motusalternatus.client.gui;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.fml.client.config.IConfigElement;
import weissmoon.motusalternatus.ConfigurationHandler;
import weissmoon.motusalternatus.Reference;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 9/25/19.
 */
public class ModGuiConfig extends GuiConfig {
    public ModGuiConfig (GuiScreen guiScreen){
        super(guiScreen, getConfigElements(),
                //new ConfigElement(ConfigurationHandler.configuration.getCategory("general")).getChildElements(),
                Reference.MOD_ID,
                false,
                false,
                GuiConfig.getAbridgedConfigPath(ConfigurationHandler.configuration.toString()));
    }

    public static List<IConfigElement> getConfigElements(){
        List<IConfigElement> list = new ArrayList();

        list.add(new ConfigElement(ConfigurationHandler.configuration.getCategory(Configuration.CATEGORY_CLIENT)));

        return list;
    }
}
