package weissmoon.motusalternatus.client;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Created by Weissmoon on 9/25/19.
 */
@SideOnly(Side.CLIENT)
public class ClientEvents {

    @SubscribeEvent
    public void onFOVUpdate(FOVUpdateEvent event){
//        IAttributeInstance iattributeinstance = Minecraft.getMinecraft().player.getAttributeMap().getAttributeInstance(SharedMonsterAttributes.MOVEMENT_SPEED);
//        iattributeinstance.removeModifier(MotionEvents.uuid);
//        double mod = iattributeinstance.getAttributeValue();
//        //mod -= 1 + iattributeinstance.getModifier(MotionEvents.uuid).getAmount();
//
//        float f = event.getFov();
//        f = f - (f - (float)(f * (((mod) / (double) event.getEntity().capabilities.getWalkSpeed() + 1.0D) / 2.0D)));
        event.setNewfov(1);
    }
}
