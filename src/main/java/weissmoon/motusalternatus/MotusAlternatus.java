package weissmoon.motusalternatus;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLConstructionEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import org.apache.logging.log4j.Logger;
import weissmoon.motusalternatus.client.ClientEvents;

import java.io.File;

/**
 * Created by Weissmoon on 10/29/18.
 */
@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.VERSION, guiFactory = Reference.GUI_FACTORY_CLASS, acceptableRemoteVersions = "*")
public class MotusAlternatus{

    private ClientEvents clientEvents;
    @Instance
    public static MotusAlternatus instance;
    Logger log;

    @EventHandler
    public void constructing (FMLConstructionEvent event) {
    }

    @EventHandler
    public void preInit(FMLPreInitializationEvent event){
        ConfigurationHandler.init(new File(event.getModConfigurationDirectory().getAbsolutePath() + File.separator + Reference.AUTHOR + File.separator + Reference.MOD_ID + ".cfg"));
        MinecraftForge.EVENT_BUS.register(ConfigurationHandler.class);
        MinecraftForge.EVENT_BUS.register(new MotionEvents());
        handleFOVSwitch();
    }

    public void handleFOVSwitch(){
        if(FMLCommonHandler.instance().getSide() == Side.CLIENT) {
            if(ConfigurationHandler.noFOV){
                MinecraftForge.EVENT_BUS.unregister(clientEvents);
                clientEvents = null;
            }else{
                clientEvents = new ClientEvents();
                MinecraftForge.EVENT_BUS.register(clientEvents);
            }
        }
    }
}
